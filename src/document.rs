use chrono::offset::Utc;
use chrono::Date;
use crate::Author;

#[derive(Default)]
pub struct Document {
    pub author: Vec<Author>,
    pub title: String,
    pub volume: Option<i32>,     // Volume number
    pub version: Option<String>, // Book version
    pub source: Option<String>,  // Source of this document
    pub publisher: Option<String>,
    pub pub_date: Option<Date<Utc>>,
    pub location: Option<String>,
}

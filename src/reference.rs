use chrono::offset::Utc;
use chrono::Date;
use crate::Document;
use url::Url;

pub enum Reference {
    Document(Document),
    Url { url: Url, referenced_on: Date<Utc> },
}

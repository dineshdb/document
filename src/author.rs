#[derive(Default)]
pub struct Author {
    pub fname: String,
    pub lname: String,
    pub mname: Option<String>,
}
